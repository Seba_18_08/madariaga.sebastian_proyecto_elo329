#include "esp32socket.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    ESP32Socket w;
    w.show();
    return a.exec();
}
