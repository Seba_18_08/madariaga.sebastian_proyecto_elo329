#ifndef ESTABLECERCONEXION_H
#define ESTABLECERCONEXION_H

#include <QDialog>

namespace Ui {
class EstablecerConexion;
}

class EstablecerConexion : public QDialog
{
    Q_OBJECT

public:
    explicit EstablecerConexion(QWidget *parent = nullptr);
    ~EstablecerConexion();
    QString hostname() const;
    quint16 port() const;
private slots:
    void on_Connect_clicked();
    void on_cancel_clicked();
signals:
    void connected();
    void disconnected();
private:
    Ui::EstablecerConexion *ui;
    QString mHostname;
    quint16 mPort;
};
// permite generar las funciones inline fuera de las demás funciones miembro
inline QString EstablecerConexion::hostname()const{
    return mHostname;
}
inline quint16 EstablecerConexion::port()const{
    return mPort;
}

#endif // ESTABLECERCONEXION_H
