#ifndef ESP32SOCKET_H
#define ESP32SOCKET_H

#include <QMainWindow>
#include "establecerconexion.h"
/*Librerias para generar cliente de red*/
#include <QTcpSocket>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
/*Librerias para leer documento JSON*/
#include <QJsonDocument>
#include <QJsonObject>


QT_BEGIN_NAMESPACE
namespace Ui { class ESP32Socket; }
QT_END_NAMESPACE

class QTcpSocket;

class ESP32Socket : public QMainWindow
{
    Q_OBJECT

public:
    ESP32Socket(QWidget *parent = nullptr);
    ~ESP32Socket();
    void makeHttpRequest();
public slots:
    void on_Conexion_clicked();
    void onReplyReceived(QNetworkReply *reply);
signals:
    void EstadoLed();
    void valorTempe();
private:
    bool conectado;
    Ui::ESP32Socket *ui;
    QTcpSocket *mSocket;
    EstablecerConexion *Establecercone;
    QNetworkAccessManager *mNetworkManager;
};
#endif // ESP32SOCKET_H
