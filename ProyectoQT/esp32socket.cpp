#include "esp32socket.h"
#include "ui_esp32socket.h"
#include "establecerconexion.h"
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QJsonDocument>
#include <QJsonObject>
#include <QTimer.h>

ESP32Socket::ESP32Socket(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::ESP32Socket)
{
    conectado = false;
    ui->setupUi(this);
    mNetworkManager = new QNetworkAccessManager(this);

    connect(mNetworkManager, &QNetworkAccessManager::finished, this, &ESP32Socket::onReplyReceived);

 }

ESP32Socket::~ESP32Socket()
{
    delete ui;
}

void ESP32Socket::on_Conexion_clicked()
{
    makeHttpRequest();

}
void ESP32Socket::makeHttpRequest(){
    EstablecerConexion D(this);
    if (D.exec() == QDialog::Rejected) {
        qDebug() << "Rechazado";
        conectado = false;
        return;
    }

    // Realizar solicitud HTTP GET al servidor del ESP32
    QString urlStr = "http://" + D.hostname(); // Reemplaza "http://" con el protocolo correcto
    QUrl url(urlStr);
    QNetworkRequest request(url);
    mNetworkManager->get(request);
}

void ESP32Socket::onReplyReceived(QNetworkReply *reply)
{
    if (reply->error() == QNetworkReply::NoError) {
        QByteArray jsonData = reply->readAll();

        // Procesar el archivo JSON
        QJsonDocument jsonDoc = QJsonDocument::fromJson(jsonData);
        if (!jsonDoc.isNull()) {
            if (jsonDoc.isObject()) {
                QJsonObject jsonObj = jsonDoc.object();

                // Acceder a los valores del archivo JSON y ejecutar acciones según sea necesario
                // Por ejemplo:
                if (jsonObj.contains("temperatura") && jsonObj.contains("LedStatus") /*&& jsonObj.contains("humedad")*/) {
                    QString LedStatus = jsonObj.value("LedStatus").toString();
                    double temperatura = jsonObj.value("temperatura").toDouble();

                    // Ejemplo de uso de los datos del archivo JSON
                    qDebug() << "Temperatura:" << temperatura;
                    qDebug() << "LedStatus:" << LedStatus;
                    ui->TemperatureLabel->setText(QString::number(temperatura));
                    ui->LedStateLcd->setText(LedStatus);
                }
            }
        }
    } else {
        qDebug() << "Error en la solicitud:" << reply->errorString();
    }

    reply->deleteLater();
}
