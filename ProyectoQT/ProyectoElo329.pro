#QT       += core gui network
QT       += core gui network widgets
QT       += widgets
QT       += network
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


# Generar el archivo moc_esp32socket.cpp
#moc_headers.files = $$HEADERS
#moc_headers.path = .moc
#DEPLOYMENT += moc_headers

SOURCES += \
    establecerconexion.cpp \
    main.cpp \
    esp32socket.cpp\
#    moc_esp32socket.cpp

HEADERS += \
    esp32socket.h \
    establecerconexion.h

FORMS += \
    esp32socket.ui \
    establecerconexion.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    diagramaclienteservidor.qmodel \
    diagramadeclases.qmodel
