#include "establecerconexion.h"
#include "ui_establecerconexion.h"

EstablecerConexion::EstablecerConexion(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EstablecerConexion)
{
    ui->setupUi(this);

}

EstablecerConexion::~EstablecerConexion()
{
    delete ui;
}

void EstablecerConexion::on_Connect_clicked()
{
    //guarda la Ip y puerto del servicio del microcontrolador
    mHostname = ui->HostName->text();
    mPort = ui->Port->value();
    accept();
}


void EstablecerConexion::on_cancel_clicked()
{
    reject();
}
