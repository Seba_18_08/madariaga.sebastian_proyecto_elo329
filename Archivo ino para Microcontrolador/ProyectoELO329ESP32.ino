******************************************************************************
* Proyecto - Programación Orientada a Objetos                        ELO 329
* Sebastián Madariaga Navarro      201821039-0                   05 Julio 2022
******************************************************************************
//Archivo .ino para microcontrolador ESP32

#include "WiFi.h"
#include <WiFiClient.h>
#include <Wire.h>
#include "SPIFFS.h"
#include <PubSubClient.h>
#include <ArduinoJson.h>
#include "esp_system.h"

// Información del Wifi
const char* ssid = "ELO329PROYECTO";
const char* password = "elo329Proyecto";

// El Web Server utiliza el puerto 80
WiFiServer server(80);
int valor = 0;
char c;
int value = 0;
bool presionado = false;

void buttonpresionado() {
  presionado = !presionado; // Establecer el flag de botón presionado en true o false
  if(presionado){
    Serial.println("Boton presionado");
    value = 1;
    digitalWrite(2,HIGH);
    delay(500);
  }else{
    value = 0;
    digitalWrite(2,LOW);
    delay(500);
  }
}

void setup() {
  // Iniciando velocidad del Puerto Serial
  Serial.begin(115200);
  delay(10);
  pinMode(2, OUTPUT);
  pinMode(35, INPUT); //Sensor de Temperatura
  pinMode(19, INPUT); //Boton
  
  // Conectando a la red WiFi
  Serial.println();
  Serial.print("Conectando a ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi conectado");

  // Iniciando el servidor web
  server.begin();
  Serial.println("Web server ejecutándose. Esperando a la ESP IP...");
  delay(10000);

  // Mostrar la dirección IP de la ESP en el Monitor Serie
  Serial.println(WiFi.localIP());
}

void loop() {
  // Esperando nuevas conexiones (clientes)
  WiFiClient client = server.available();

  if (client) {
    Serial.println("Nuevo cliente");

    //Mientras haya un cliente conectado se genera JSON y se envia
    while (client.connected()) {
      if(digitalRead(19)>0){
        buttonpresionado();
      }
      // Crear un objeto JSON
      valor = analogRead(35);
      StaticJsonDocument<200> doc;
      //doc["sensor"] = "DHT22";
      doc["temperatura"] = 18;
      doc["LedStatus"] = "Prendido";
/*      if (value = 1){
        doc["LedStatus"] = "Prendido";
      }else{
        doc["LedStatus"] = "Apagado";
      }*/

      // Convertir el objeto JSON a una cadena
      String jsonStr;
      serializeJson(doc, jsonStr);

      // Enviar la cadena JSON al cliente
      client.println("HTTP/1.1 200 OK");
      client.println("Content-Type: application/json");
      client.println("Connection: keep-alive");
      client.println("Access-Control-Allow-Origin: *"); // Permitir peticiones desde cualquier origen
      client.println("Content-Length: " + String(jsonStr.length()));
      client.println();
      client.println(jsonStr);

      Serial.println("Datos enviados correctamente");

      // Esperar 500 milisegundoa antes de enviar nuevos datos
      delay(500);
    }

    // Cerrando la conexión con el cliente solo cuando se desconecte
    client.stop();
    Serial.println("Cliente desconectado");
  }
}
