******************************************************************************
* Proyecto - Programación Orientada a Objetos                        ELO 329
* Sebastián Madariaga Navarro      201821039-0                   05 Julio 2022
******************************************************************************
## Desarrollo del proyecto
Problema: El uso de dispositivos embebidos se ha vuelto esencial en el último tiempo, sin embargo, poseen de algunas limitaciones a la hora de codificar con ellos. Limitaciones en memoria, en lenguaje de programación y se suele necesitar eficiencia energética (es decir usar la menor implementación posible).

Solución: Implementar un sistema servidor-cliente, en donde datos tomados por un microcontrolador (ESP32 en este caso) sean trasmitidos vía HTTP hasta una plataforma de más alto nivel (QT creator en este caso), para ahí generar una interfaz grafica donde poder desarrollar interacción con los datos tomados. Se crean servicios en el microcontrolador y la interfaz de QT actúa como un socket donde se recibe la información. La solución implementada considera una red local, donde los datos son enviados vía WiFi.

En palabras más simples, el microcontrolador hace toma de mediciones de datos y espera que un cliente se conecte a la IP y puerto del servidor, establecido por el dispositivo embebido. Una vez que se conecte un cliente, se procede a hacer envíos de los datos actuales tomados mediante un archivo JSON.

## Codigo .ino
Este código está diseñado para un microcontrolador ESP32. Está desarrollado en el IDE de arduino
el cual puede descargar en el siguiente link:

https://www.arduino.cc/en/software

## Codigos cpp
El proyecto desarrolla la interfaz gráfica en QT creator, para poder hacer uso de este programa 
se debe abrir mediante el IDE de QT creator, en especifico importar el archivo .pro, encontrado 
en el repositorio.

El desarrollo del proyecto es explicado a mayor detalle en la documentación en .html presente en 
el repositorio, Madariaga.Sebastian_ProyectoELO329.
